<?php

use App\Http\Controllers\V1;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function () {
    Route::prefix('animal_kinds')->group(function () {
        Route::get('/', V1\AnimalKind\Actions\IndexAction::class);
    });

    Route::prefix('animals')->group(function () {
        Route::get('/', V1\Animal\Actions\IndexAction::class);
        Route::post('/', V1\Animal\Actions\CreateAction::class);
        Route::post('/age', V1\Animal\Actions\UpdateAgeAction::class);
    });
});
