<?php

declare(strict_types=1);

namespace Tests\Feature\Api\V1\Animal;

use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

final class CreateActionTest extends TestCase
{
    private const API_URI = '/api/v1/animals/';

    public function testAnimalCreatedSuccessfully(): void
    {
        $this->json('post', self::API_URI, ['kindId' => 1])
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'id',
                    'status',
                    'age',
                    'size',
                    'created_at',
                    'updated_at',
                    'kind' => [
                        'id',
                        'kind',
                        'avatar',
                        'name',
                        'start_age',
                        'max_age',
                        'start_size',
                        'max_size',
                        'growth_factor',
                        'created_at',
                        'updated_at',
                    ],
                ]
            );

        $this->assertDatabaseHas('animals', [
            'animal_kind_id' => 1,
            'status'         => 0,
            'age'            => 1.0,
            'size'           => 50.0,
        ]);
    }

    public function testAnimalKindNotFound(): void
    {
        $this->json('post', self::API_URI, ['kindId' => 7])
            ->assertStatus(Response::HTTP_NOT_FOUND)
            ->assertJson(
                [
                    'error' => 'Ошибка! Такого типа животного не существует.',
                ]
            );

        $this->assertDatabaseMissing('animals', [
            'animal_kind_id' => 7,
        ]);
    }

    public function testAnimalAlreadyExists(): void
    {
        $this->json('post', self::API_URI, ['kindId' => 1])
            ->assertStatus(Response::HTTP_CONFLICT)
            ->assertJson(
                [
                    'error' => 'Ошибка! Такое животное уже существует.',
                ]
            );
    }
}
