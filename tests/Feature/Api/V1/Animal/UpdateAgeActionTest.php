<?php

declare(strict_types=1);

namespace Tests\Feature\Api\V1\Animal;

use App\Domain\Models\Animal;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

final class UpdateAgeActionTest extends TestCase
{
    private const API_URI = '/api/v1/animals/age';

    public function testUpdatedAgeSuccessfully(): void
    {
        $this->json('post', self::API_URI, ['id' => 1])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    'id',
                    'status',
                    'age',
                    'size',
                    'created_at',
                    'updated_at',
                    'kind' => [
                        'id',
                        'kind',
                        'avatar',
                        'name',
                        'start_age',
                        'max_age',
                        'start_size',
                        'max_size',
                        'growth_factor',
                        'created_at',
                        'updated_at',
                    ],
                ]
            );

        $this->assertDatabaseHas('animals', [
            'animal_kind_id' => 1,
            'status'         => 0,
            'age'            => 1.2,
            'size'           => 60.0,
        ]);
    }

    public function testAnimalNotFound(): void
    {
        $this->json('post', self::API_URI, ['id' => 10])
            ->assertStatus(Response::HTTP_NOT_FOUND)
            ->assertJson(
                [
                    'error' => 'Ошибка! Такого животного не существует.',
                ]
            );
    }

    public function testAnimalHasGrown(): void
    {
        $animal = Animal::find(1);

        $animal->status = 1;
        $animal->age    = 15;
        $animal->size   = 400;

        $animal->save();

        $this->json('post', self::API_URI, ['id' => 1])
            ->assertStatus(Response::HTTP_CONFLICT)
            ->assertJson(
                [
                    'error' => 'Ошибка! Животное достигло максимального возраста и размера.',
                ]
            );

        $this->assertDatabaseHas('animals', [
            'animal_kind_id' => 1,
            'status'         => 1,
            'age'            => 15.0,
            'size'           => 400.0,
        ]);
    }
}
