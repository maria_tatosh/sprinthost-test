<?php

namespace Database\Seeders;

use DateTime;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AnimalKindSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getData() as $animalKind) {
            DB::table('animal_kinds')->insert(
                [
                    'kind'          => $animalKind['kind'],
                    'avatar'        => $animalKind['avatar'],
                    'name'          => $animalKind['name'],
                    'start_age'     => $animalKind['start_age'],
                    'max_age'       => $animalKind['max_age'],
                    'start_size'    => $animalKind['start_size'],
                    'max_size'      => $animalKind['max_size'],
                    'growth_factor' => $animalKind['growth_factor'],
                    'created_at'    => new DateTime(),
                    'updated_at'    => new DateTime(),
                ]
            );
        }
    }

    private function getData(): array
    {
        return [
            [
                'kind'          => 'cat',
                'avatar'        => '/images/cat_1.png',
                'name'          => 'Berry',
                'start_age'     => 1,
                'max_age'       => 15,
                'start_size'    => 50,
                'max_size'      => 400,
                'growth_factor' => 1.2,
            ],
            [
                'kind'          => 'dog',
                'avatar'        => '/images/dog.png',
                'name'          => 'Marta',
                'start_age'     => 1,
                'max_age'       => 15,
                'start_size'    => 40,
                'max_size'      => 380,
                'growth_factor' => 1.1,
            ],
            [
                'kind'          => 'cat',
                'avatar'        => '/images/cat_2.png',
                'name'          => 'Bobby',
                'start_age'     => 1,
                'max_age'       => 14,
                'start_size'    => 45,
                'max_size'      => 300,
                'growth_factor' => 1.15,
            ],
            [
                'kind'          => 'parrot',
                'avatar'        => '/images/parrot.png',
                'name'          => 'Izzy',
                'start_age'     => 1,
                'max_age'       => 7,
                'start_size'    => 60,
                'max_size'      => 350,
                'growth_factor' => 1.1,
            ],
        ];
    }
}
