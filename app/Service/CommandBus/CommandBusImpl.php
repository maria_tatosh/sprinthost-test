<?php

declare(strict_types=1);

namespace App\Service\CommandBus;

use Illuminate\Bus\Dispatcher;

final class CommandBusImpl implements CommandBus
{
    /** @var \Illuminate\Bus\Dispatcher */
    private Dispatcher $dispatcher;

    /**
     * @param  \Illuminate\Bus\Dispatcher  $dispatcher
     */
    public function __construct(Dispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function dispatch(mixed $command): mixed
    {
        return $this->dispatcher->dispatch($command);
    }

    public function map(array $map): void
    {
        $this->dispatcher->map($map);
    }
}
