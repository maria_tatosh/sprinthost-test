<?php

declare(strict_types=1);

namespace App\Service\CommandBus;

interface CommandBus
{
    /**
     * @param  mixed  $command
     *
     * @return mixed
     */
    public function dispatch(mixed $command): mixed;

    /**
     * @param  array  $map
     *
     * @return void
     */
    public function map(array $map): void;
}
