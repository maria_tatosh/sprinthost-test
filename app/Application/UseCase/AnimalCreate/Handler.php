<?php

declare(strict_types=1);

namespace App\Application\UseCase\AnimalCreate;

use App\Application\Exception\Animal\AnimalAlreadyExists;
use App\Application\Repository\AnimalKindRepository;
use App\Domain\Models\Animal;

final class Handler
{
    /** @var \App\Application\Repository\AnimalKindRepository */
    private AnimalKindRepository $animalKindRepository;

    /**
     * @param  \App\Application\Repository\AnimalKindRepository  $animalKindRepository
     */
    public function __construct(AnimalKindRepository $animalKindRepository)
    {
        $this->animalKindRepository = $animalKindRepository;
    }

    /**
     * @param  \App\Application\UseCase\AnimalCreate\Command  $command
     *
     * @throws \App\Application\Exception\AnimalKind\AnimalKindNotFound
     * @throws \App\Application\Exception\Animal\AnimalAlreadyExists
     */
    public function handle(Command $command): void
    {
        $animalKind = $this->animalKindRepository->get($command->getKindId());

        if ($animalKind->animal()->exists()) {
            throw new AnimalAlreadyExists($animalKind->animal->id);
        }

        $animal = new Animal();

        $animal->age    = $animalKind->start_age;
        $animal->size   = $animalKind->start_size;
        $animal->status = Animal::STATUS_CREATED;

        $animalKind->animal()->save($animal);

        $command->setId($animal->id);
    }
}
