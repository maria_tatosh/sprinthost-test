<?php

declare(strict_types=1);

namespace App\Application\UseCase\AnimalCreate;

final class Command
{
    /** @var int|null */
    private ?int $id;

    /** @var int */
    private int $kindId;

    /**
     * @param  int  $kindId
     */
    public function __construct(int $kindId)
    {
        $this->id     = null;
        $this->kindId = $kindId;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param  int  $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getKindId(): int
    {
        return $this->kindId;
    }
}
