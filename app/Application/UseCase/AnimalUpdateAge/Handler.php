<?php

declare(strict_types=1);

namespace App\Application\UseCase\AnimalUpdateAge;

use App\Application\Exception\Animal\AnimalHasGrown;
use App\Application\Repository\AnimalRepository;
use App\Domain\Models\Animal;

final class Handler
{
    /** @var \App\Application\Repository\AnimalRepository */
    private AnimalRepository $animalRepository;

    /**
     * @param  \App\Application\Repository\AnimalRepository  $animalRepository
     */
    public function __construct(AnimalRepository $animalRepository)
    {
        $this->animalRepository = $animalRepository;
    }

    /**
     * @param  \App\Application\UseCase\AnimalUpdateAge\Command  $command
     *
     * @return int|null
     * @throws \App\Application\Exception\Animal\AnimalHasGrown
     * @throws \App\Application\Exception\Animal\AnimalNotFound
     */
    public function handle(Command $command): ?int
    {
        $animal = $this->animalRepository->get($command->getId());

        if ($animal->isGrown()) {
            throw new AnimalHasGrown($animal->kind->name);
        }

        if (! $this->canMutateAge($animal) || ! $this->canMutateSize($animal)) {
            $animal->status = Animal::STATUS_GROWN;
        } else {
            $animal->age  *= $animal->kind->growth_factor;
            $animal->size *= $animal->kind->growth_factor;
        }

        $this->animalRepository->save($animal);

        return $animal->id;
    }

    /**
     * @param  \App\Domain\Models\Animal  $animal
     *
     * @return bool
     */
    private function canMutateAge(Animal $animal): bool
    {
        return $animal->age * $animal->kind->growth_factor < $animal->kind->max_age;
    }

    /**
     * @param  \App\Domain\Models\Animal  $animal
     *
     * @return bool
     */
    private function canMutateSize(Animal $animal): bool
    {
        return $animal->size * $animal->kind->growth_factor < $animal->kind->max_size;
    }
}
