<?php

declare(strict_types=1);

namespace App\Application\UseCase\AnimalUpdateAge;

final class Command
{
    /** @var int */
    private int $id;

    /**
     * @param  int  $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
}
