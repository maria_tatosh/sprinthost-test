<?php

declare(strict_types=1);

namespace App\Application\Repository;

use App\Domain\Models\AnimalKind;

interface AnimalKindRepository
{
    /**
     * @return \App\Domain\Models\AnimalKind[]
     */
    public function getAll(): array;

    /**
     * @param  int  $id
     *
     * @return \App\Domain\Models\AnimalKind
     * @throws \App\Application\Exception\AnimalKind\AnimalKindNotFound
     */
    public function get(int $id): AnimalKind;

    /**
     * @param  \App\Domain\Models\AnimalKind  $animalKind
     *
     * @return bool
     */
    public function hasAnimals(AnimalKind $animalKind): bool;
}
