<?php

declare(strict_types=1);

namespace App\Application\Repository;

use App\Domain\Models\Animal;

interface AnimalRepository
{
    /**
     * @return \App\Domain\Models\Animal[]
     */
    public function getAll(): array;

    /**
     * @param  int  $id
     *
     * @return \App\Domain\Models\Animal
     * @throws \App\Application\Exception\Animal\AnimalNotFound
     */
    public function get(int $id): Animal;

    /**
     * @param  \App\Domain\Models\Animal  $animal
     *
     * @return void
     */
    public function save(Animal $animal): void;
}
