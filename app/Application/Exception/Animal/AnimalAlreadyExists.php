<?php

declare(strict_types=1);

namespace App\Application\Exception\Animal;

use Exception;

final class AnimalAlreadyExists extends Exception
{
    /**
     * @param  int  $id
     */
    public function __construct(int $id)
    {
        parent::__construct(sprintf('Animal "%d" already exists', $id));
    }
}
