<?php

declare(strict_types=1);

namespace App\Application\Exception\Animal;

use Exception;

final class AnimalHasGrown extends Exception
{
    /**
     * @param  string  $id
     */
    public function __construct(string $id)
    {
        parent::__construct(sprintf('Animal %s has grown', $id));
    }
}
