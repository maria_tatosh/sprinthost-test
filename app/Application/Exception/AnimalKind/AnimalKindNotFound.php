<?php

declare(strict_types=1);

namespace App\Application\Exception\AnimalKind;

use Exception;

final class AnimalKindNotFound extends Exception
{
    /**
     * @param  int  $id
     */
    public function __construct(int $id)
    {
        parent::__construct(sprintf('Animal kind "%d" not found', $id));
    }
}
