<?php

declare(strict_types=1);

namespace App\Adapter\Repository;

use App\Application\Exception\Animal\AnimalNotFound;
use App\Application\Repository\AnimalRepository;
use App\Domain\Models\Animal;

final class AnimalRepositoryImpl implements AnimalRepository
{
    public function getAll(): array
    {
        return Animal::with('kind')->get()->toArray();
    }

    public function get(int $id): Animal
    {
        $animal = Animal::with('kind')->find($id);

        if (! $animal instanceof Animal) {
            throw new AnimalNotFound($id);
        }

        return $animal;
    }

    public function save(Animal $animal): void
    {
        $animal->save();
    }
}
