<?php

declare(strict_types=1);

namespace App\Adapter\Repository;

use App\Application\Exception\AnimalKind\AnimalKindNotFound;
use App\Application\Repository\AnimalKindRepository;
use App\Domain\Models\AnimalKind;

final class AnimalKindRepositoryImpl implements AnimalKindRepository
{
    public function getAll(): array
    {
        return AnimalKind::all()->toArray();
    }

    public function get(int $id): AnimalKind
    {
        $animalKind = AnimalKind::find($id);

        if (! $animalKind instanceof AnimalKind) {
            throw new AnimalKindNotFound($id);
        }

        return $animalKind;
    }

    public function hasAnimals(AnimalKind $animalKind): bool
    {
        return $animalKind->animals()->exists();
    }
}
