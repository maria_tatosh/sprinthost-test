<?php

declare(strict_types=1);

namespace App\Domain\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Animal extends Model
{
    use HasFactory;

    public const STATUS_CREATED = 0;
    public const STATUS_GROWN   = 1;

    /**
     * @return bool
     */
    public function isGrown(): bool
    {
        return $this->status === self::STATUS_GROWN;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kind(): BelongsTo
    {
        return $this->belongsTo(AnimalKind::class, 'animal_kind_id', 'id');
    }
}
