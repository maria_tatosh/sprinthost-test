<?php

declare(strict_types=1);

namespace App\Http\Controllers\Common;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class Request extends FormRequest
{
    protected $stopOnFirstFailure = true;

    public function failedValidation(Validator $validator)
    {
        if ($this->expectsJson()) {
            return response()->json((new ValidationException($validator))->getMessage(), 422);
        }

        parent::failedValidation($validator);
    }
}
