<?php

declare(strict_types=1);

namespace App\Http\Controllers\V1\AnimalKind\Actions;

use App\Application\Repository\AnimalKindRepository;
use App\Http\Controllers\Common\Action;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

final class IndexAction extends Action
{
    /** @var \App\Application\Repository\AnimalKindRepository */
    private AnimalKindRepository $animalKindsRepository;

    /**
     * @param  \App\Application\Repository\AnimalKindRepository  $animalKindsRepository
     */
    public function __construct(AnimalKindRepository $animalKindsRepository)
    {
        $this->animalKindsRepository = $animalKindsRepository;
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request): JsonResponse
    {
        return new JsonResponse($this->animalKindsRepository->getAll());
    }
}
