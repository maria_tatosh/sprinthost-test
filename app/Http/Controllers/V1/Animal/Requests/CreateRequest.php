<?php

declare(strict_types=1);

namespace App\Http\Controllers\V1\Animal\Requests;

use App\Http\Controllers\Common\Request;

final class CreateRequest extends Request
{
    /**
     * @return int
     */
    public function getKindId(): int
    {
        return $this->get('kindId');
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [];
    }
}
