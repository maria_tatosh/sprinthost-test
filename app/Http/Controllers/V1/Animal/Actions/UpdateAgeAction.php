<?php

declare(strict_types=1);

namespace App\Http\Controllers\V1\Animal\Actions;

use App\Application\Exception\Animal\AnimalHasGrown;
use App\Application\Exception\Animal\AnimalNotFound;
use App\Application\Repository\AnimalRepository;
use App\Application\UseCase\AnimalUpdateAge;
use App\Http\Controllers\Common\Action;
use App\Http\Controllers\V1\Animal\Requests\UpdateAgeRequest;
use App\Service\CommandBus\CommandBus;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class UpdateAgeAction extends Action
{
    /** @var \App\Service\CommandBus\CommandBus */
    private CommandBus $commandBus;

    /** @var \App\Application\Repository\AnimalRepository */
    private AnimalRepository $animalRepository;

    /**
     * @param  \App\Service\CommandBus\CommandBus            $commandBus
     * @param  \App\Application\Repository\AnimalRepository  $animalRepository
     */
    public function __construct(CommandBus $commandBus, AnimalRepository $animalRepository)
    {
        $this->commandBus       = $commandBus;
        $this->animalRepository = $animalRepository;
    }

    /**
     * @param  \App\Http\Controllers\V1\Animal\Requests\UpdateAgeRequest  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(UpdateAgeRequest $request): JsonResponse
    {
        try {
            $this->commandBus->dispatch(new AnimalUpdateAge\Command($request->getId()));
        } catch (AnimalNotFound $e) {
            return new JsonResponse(
                ['error' => __('errors.animal.notFound')],
                Response::HTTP_NOT_FOUND
            );
        } catch (AnimalHasGrown $e) {
            return new JsonResponse(
                ['error' => __('errors.animal.hasGrown')],
                Response::HTTP_CONFLICT
            );
        }

        return new JsonResponse($this->animalRepository->get($request->getId()));
    }
}
