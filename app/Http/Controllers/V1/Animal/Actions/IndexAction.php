<?php

declare(strict_types=1);

namespace App\Http\Controllers\V1\Animal\Actions;

use App\Application\Repository\AnimalRepository;
use App\Http\Controllers\Common\Action;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

final class IndexAction extends Action
{
    /** @var \App\Application\Repository\AnimalRepository */
    private AnimalRepository $animalRepository;

    /**
     * @param  \App\Application\Repository\AnimalRepository  $animalRepository
     */
    public function __construct(AnimalRepository $animalRepository)
    {
        $this->animalRepository = $animalRepository;
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request): JsonResponse
    {
        return new JsonResponse($this->animalRepository->getAll());
    }
}
