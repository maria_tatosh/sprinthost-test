<?php

declare(strict_types=1);

namespace App\Http\Controllers\V1\Animal\Actions;

use App\Application\Exception\Animal\AnimalAlreadyExists;
use App\Application\Exception\AnimalKind\AnimalKindNotFound;
use App\Application\Repository\AnimalRepository;
use App\Application\UseCase\AnimalCreate;
use App\Http\Controllers\Common\Action;
use App\Http\Controllers\V1\Animal\Requests\CreateRequest;
use App\Service\CommandBus\CommandBus;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class CreateAction extends Action
{
    /** @var \App\Service\CommandBus\CommandBus */
    private CommandBus $commandBus;

    /** @var \App\Application\Repository\AnimalRepository */
    private AnimalRepository $animalRepository;

    /**
     * @param  \App\Service\CommandBus\CommandBus            $commandBus
     * @param  \App\Application\Repository\AnimalRepository  $animalRepository
     */
    public function __construct(CommandBus $commandBus, AnimalRepository $animalRepository)
    {
        $this->commandBus       = $commandBus;
        $this->animalRepository = $animalRepository;
    }

    /**
     * @param  \App\Http\Controllers\V1\Animal\Requests\CreateRequest  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(CreateRequest $request): JsonResponse
    {
        $command = new AnimalCreate\Command($request->getKindId());

        try {
            $this->commandBus->dispatch($command);
        } catch (AnimalKindNotFound $e) {
            return new JsonResponse(
                ['error' => __('errors.animalKind.notFound')],
                Response::HTTP_NOT_FOUND
            );
        } catch (AnimalAlreadyExists $e) {
            return new JsonResponse(
                ['error' => __('errors.animal.alreadyExists')],
                Response::HTTP_CONFLICT
            );
        }

        return new JsonResponse(
            $this->animalRepository->get($command->getId()),
            Response::HTTP_CREATED
        );
    }
}
