<?php

namespace App\Providers;

use App\Adapter\Repository\AnimalKindRepositoryImpl;
use App\Adapter\Repository\AnimalRepositoryImpl;
use App\Application\Repository\AnimalKindRepository;
use App\Application\Repository\AnimalRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(AnimalRepository::class, AnimalRepositoryImpl::class);
        $this->app->singleton(AnimalKindRepository::class, AnimalKindRepositoryImpl::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
