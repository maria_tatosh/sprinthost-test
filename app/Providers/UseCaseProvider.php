<?php

namespace App\Providers;

use App\Application\UseCase\AnimalCreate;
use App\Application\UseCase\AnimalUpdateAge;
use App\Service\CommandBus\CommandBus;
use Illuminate\Support\ServiceProvider;

class UseCaseProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function register()
    {
        /** @var \App\Service\CommandBus\CommandBus $commandBus */
        $commandBus = $this->app->make(CommandBus::class);

        $commandBus->map(
            [
                AnimalCreate\Command::class    => AnimalCreate\Handler::class,
                AnimalUpdateAge\Command::class => AnimalUpdateAge\Handler::class,
            ]
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
