import axios from 'axios';

class ApiService {
  constructor(service) {
    this.service = service;
  }

  makeURL(path) {
    return `/api/v1/${this.service}/${path ?? ''}`;
  }

  assertMethod(method) {
    if (!['GET', 'POST', 'PUT', 'DELETE'].includes(method.toUpperCase())) {
      throw new Error('Unexpected HTTP method');
    }
  }

  request({ method = 'GET', path = '', params = {}, body ={} }) {
    this.assertMethod(method);

    return axios.request({
      url: this.makeURL(path),
      method,
      params,
      data: body,
    })
  }
}

export default ApiService;