import ApiService from './ApiService';

class AnimalsApi extends ApiService {
  constructor() {
    super('animals');
  }

  async index() {
    return await this.request({});
  }

  async updateAge(id) {
    return await this.request({
      method: 'POST',
      path: 'age',
      body: { id },
    });
  }

  async create(kindId) {
    return await this.request({
      method: 'POST',
      body: { kindId },
    });
  }
}

export default AnimalsApi;
