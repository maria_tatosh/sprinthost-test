import ApiService from './ApiService';

class AnimalKindsApi extends ApiService {
  constructor() {
    super('animal_kinds');
  }

  async index() {
    return await this.request({});
  }
}

export default AnimalKindsApi;