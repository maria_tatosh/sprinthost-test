import * as Vue from 'vue';

import VueAxios from 'vue-axios';
import axios from 'axios';

import App from './src/App';

Vue
  .createApp(App)
  .use(VueAxios, axios)
  .mount('#app');
